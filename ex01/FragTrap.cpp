//
// Created by Dmitry Titenko on 11/2/17.
//

#include <iostream>
#include "FragTrap.hpp"

FragTrap::FragTrap() : hitPoints(100), maxHitPoints(100),
					   energyPoints(100), maxEnergyPoints(100),
					   level(1),
					   meleeAttackDamage(30), rangedAttackDamage(20),
					   armorDamagaeReduction(5)
{
	std::cout << "[CREATED]FR4G-TP without name" << std::endl;
}

FragTrap::FragTrap(const std::string &name) :
	hitPoints(100), maxHitPoints(100),
	energyPoints(100), maxEnergyPoints(100),
	level(1),
	meleeAttackDamage(30), rangedAttackDamage(20),
	armorDamagaeReduction(5), name(name)
{
	std::cout << "[CREATED]FR4G-TP " << name << std::endl;
}

FragTrap::FragTrap(const FragTrap &rhs)
{
	*this = rhs;
	std::cout << "[CREATED]FR4G-TP form FR4G-TP " << rhs.name << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << "[DESTRUCTED]FR4G-TP " << name << std::endl;
}

FragTrap &FragTrap::operator=(const FragTrap &rhs)
{
	if (this != &rhs)
	{
		hitPoints = rhs.hitPoints;
		maxHitPoints = rhs.maxHitPoints;
		energyPoints = rhs.energyPoints;
		maxEnergyPoints = rhs.maxEnergyPoints;
		level = rhs.level;
		meleeAttackDamage = rhs.meleeAttackDamage;
		rangedAttackDamage = rhs.rangedAttackDamage;
		armorDamagaeReduction = rhs.armorDamagaeReduction;
		name = rhs.name;
	}
	return *this;
}

int FragTrap::getHitPoints() const
{
	return hitPoints;
}

void FragTrap::setHitPoints(int hitPoints)
{
	FragTrap::hitPoints = hitPoints;
}

int FragTrap::getMaxHitPoints() const
{
	return maxHitPoints;
}

void FragTrap::setMaxHitPoints(int maxHitPoints)
{
	FragTrap::maxHitPoints = maxHitPoints;
}

int FragTrap::getEnergyPoints() const
{
	return energyPoints;
}

void FragTrap::setEnergyPoints(int energyPoints)
{
	FragTrap::energyPoints = energyPoints;
}

int FragTrap::getMaxEnergyPoints() const
{
	return maxEnergyPoints;
}

void FragTrap::setMaxEnergyPoints(int maxEnergyPoints)
{
	FragTrap::maxEnergyPoints = maxEnergyPoints;
	if (this->maxEnergyPoints < 0)
		this->maxEnergyPoints = 0;
}

int FragTrap::getLevel() const
{
	return level;
}

void FragTrap::setLevel(int level)
{
	FragTrap::level = level;
}

int FragTrap::getMeleeAttackDamage() const
{
	return meleeAttackDamage;
}

void FragTrap::setMeleeAttackDamage(int meleeAttackDamage)
{
	FragTrap::meleeAttackDamage = meleeAttackDamage;
}

int FragTrap::getRangedAttackDamage() const
{
	return rangedAttackDamage;
}

void FragTrap::setRangedAttackDamage(int rangedAttackDamage)
{
	FragTrap::rangedAttackDamage = rangedAttackDamage;
}

int FragTrap::getArmorDamagaeReduction() const
{
	return armorDamagaeReduction;
}

void FragTrap::setArmorDamagaeReduction(int armorDamagaeReduction)
{
	FragTrap::armorDamagaeReduction = armorDamagaeReduction;
}

const std::string &FragTrap::getName() const
{
	return name;
}

void FragTrap::setName(const std::string &name)
{
	FragTrap::name = name;
}


void FragTrap::rangedAttack(const std::string &target)
{
	std::cout << "FR4G-TP " << name << " attacks " << target <<
			  " at range, causing " << rangedAttackDamage << " points of damage!" << std::endl;
}

void FragTrap::meleeAttack(const std::string &target)
{
	std::cout << "FR4G-TP " << name << " attacks " << target <<
			  " with a melee, causing " << meleeAttackDamage << " points of damage!" << std::endl;
}

void FragTrap::takeDamage(unsigned int amount)
{
	int __hitPoints;
	if (hitPoints == 0)
		std::cout << "FR4G-TP " << name << " can't take more damage!" << std::endl;
	else if ((armorDamagaeReduction > amount))
		std::cout << "FR4G-TP " << name << " don't feel anything!" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + armorDamagaeReduction - amount) < 0)
			__hitPoints = 0;
		std::cout << "FR4G-TP " << name << " takes " << amount <<" points of damage!" << std::endl;
		setHitPoints(__hitPoints);
	}
}

void FragTrap::beRepaired(unsigned int amount)
{
	int __hitPoints;

	if (hitPoints == maxHitPoints)
		std::cout << "FR4G-TP " << name << " already full" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + amount) > maxHitPoints)
			__hitPoints = maxHitPoints;
		std::cout << "FR4G-TP " << name << " was repaired by " << amount << " and now has " << __hitPoints << " hit points!" << std::endl;
		setHitPoints(__hitPoints);
	}
}

void FragTrap::vaulthunter_dot_exe(std::string const &target)
{
	std::string attacks[] = {"Hat Spin", "Dive Kick", "Hat Trick", "Buzz Saw", "Flip Kick"};
	if (energyPoints > 0)
	{
		std::cout << "FR4G-TP " << name << " attacks " << target << " with a "
				  << attacks[std::rand() % 5] << "!!!" << std::endl;
		setEnergyPoints(energyPoints - 25);
	}
	else
		std::cout << "FR4G-TP " << name << " has no energy!" << std::endl;

}
