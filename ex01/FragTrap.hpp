//
// Created by Dmitry Titenko on 11/2/17.
//

#ifndef D03_FRAGTRAP_HPP
#define D03_FRAGTRAP_HPP


#include <string>

class FragTrap
{
	protected:
		int hitPoints, maxHitPoints, energyPoints, maxEnergyPoints, level,
			meleeAttackDamage, rangedAttackDamage, armorDamagaeReduction;
		std::string name;

	public:
		FragTrap();
		FragTrap(const std::string &name);
		FragTrap(const FragTrap &);
		virtual ~FragTrap();
		FragTrap &operator=(const FragTrap &);

		void rangedAttack(const std::string &target);
		void meleeAttack(const std::string &target);
		void takeDamage(unsigned int amount);
		void beRepaired(unsigned int amount);
		void vaulthunter_dot_exe(std::string const & target);

		//Getters and Setters
		int getHitPoints() const;

		void setHitPoints(int hitPoints);

		int getMaxHitPoints() const;

		void setMaxHitPoints(int maxHitPoints);

		int getEnergyPoints() const;

		void setEnergyPoints(int energyPoints);

		int getMaxEnergyPoints() const;

		void setMaxEnergyPoints(int maxEnergyPoints);

		int getLevel() const;

		void setLevel(int level);

		int getMeleeAttackDamage() const;

		void setMeleeAttackDamage(int meleeAttackDamage);

		int getRangedAttackDamage() const;

		void setRangedAttackDamage(int rangedAttackDamage);

		int getArmorDamagaeReduction() const;

		void setArmorDamagaeReduction(int armorDamagaeReduction);

		const std::string &getName() const;

		void setName(const std::string &name);
};


#endif //D03_FRAGTRAP_HPP
