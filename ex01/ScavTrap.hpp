//
// Created by Dmitry Titenko on 11/3/17.
//

#ifndef D03_SCAVTRAP_HPP
#define D03_SCAVTRAP_HPP


#include <string>

class ScavTrap
{
	protected:
		int hitPoints, maxHitPoints, energyPoints, maxEnergyPoints, level,
			meleeAttackDamage, rangedAttackDamage, armorDamagaeReduction;
		std::string name;

	public:
		ScavTrap();
		ScavTrap(const std::string &name);
		ScavTrap(const ScavTrap &);
		virtual ~ScavTrap();
		ScavTrap &operator=(const ScavTrap &);

		void rangedAttack(const std::string &target);
		void meleeAttack(const std::string &target);
		void takeDamage(unsigned int amount);
		void beRepaired(unsigned int amount);
		void challengeNewcomer();

		//Getters and Setters
		int getHitPoints() const;

		void setHitPoints(int hitPoints);

		int getMaxHitPoints() const;

		void setMaxHitPoints(int maxHitPoints);

		int getEnergyPoints() const;

		void setEnergyPoints(int energyPoints);

		int getMaxEnergyPoints() const;

		void setMaxEnergyPoints(int maxEnergyPoints);

		int getLevel() const;

		void setLevel(int level);

		int getMeleeAttackDamage() const;

		void setMeleeAttackDamage(int meleeAttackDamage);

		int getRangedAttackDamage() const;

		void setRangedAttackDamage(int rangedAttackDamage);

		int getArmorDamagaeReduction() const;

		void setArmorDamagaeReduction(int armorDamagaeReduction);

		const std::string &getName() const;

		void setName(const std::string &name);
};


#endif //D03_SCAVTRAP_HPP
