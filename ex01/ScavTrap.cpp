//
// Created by Dmitry Titenko on 11/3/17.
//

#include "ScavTrap.hpp"
#include <iostream>

ScavTrap::ScavTrap() : hitPoints(100), maxHitPoints(100),
					   energyPoints(50), maxEnergyPoints(50),
					   level(1),
					   meleeAttackDamage(20), rangedAttackDamage(15),
					   armorDamagaeReduction(3)
{
	std::cout << "[CREATED]SC4V-TP without name" << std::endl;
}

ScavTrap::ScavTrap(const std::string &name) :
	hitPoints(100), maxHitPoints(100),
	energyPoints(100), maxEnergyPoints(100),
	level(1),
	meleeAttackDamage(30), rangedAttackDamage(20),
	armorDamagaeReduction(5), name(name)
{
	std::cout << "[CREATED]SC4V-TP " << name << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap &rhs)
{
	*this = rhs;
	std::cout << "[CREATED]SC4V-TP form SC4V-TP " << rhs.name << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << "[DESTRUCTED]SC4V-TP " << name << std::endl;
}

ScavTrap &ScavTrap::operator=(const ScavTrap &rhs)
{
	if (this != &rhs)
	{
		hitPoints = rhs.hitPoints;
		maxHitPoints = rhs.maxHitPoints;
		energyPoints = rhs.energyPoints;
		maxEnergyPoints = rhs.maxEnergyPoints;
		level = rhs.level;
		meleeAttackDamage = rhs.meleeAttackDamage;
		rangedAttackDamage = rhs.rangedAttackDamage;
		armorDamagaeReduction = rhs.armorDamagaeReduction;
		name = rhs.name;
	}
	return *this;
}

int ScavTrap::getHitPoints() const
{
	return hitPoints;
}

void ScavTrap::setHitPoints(int hitPoints)
{
	ScavTrap::hitPoints = hitPoints;
}

int ScavTrap::getMaxHitPoints() const
{
	return maxHitPoints;
}

void ScavTrap::setMaxHitPoints(int maxHitPoints)
{
	ScavTrap::maxHitPoints = maxHitPoints;
}

int ScavTrap::getEnergyPoints() const
{
	return energyPoints;
}

void ScavTrap::setEnergyPoints(int energyPoints)
{
	ScavTrap::energyPoints = energyPoints;
}

int ScavTrap::getMaxEnergyPoints() const
{
	return maxEnergyPoints;
}

void ScavTrap::setMaxEnergyPoints(int maxEnergyPoints)
{
	ScavTrap::maxEnergyPoints = maxEnergyPoints;
	if (this->maxEnergyPoints < 0)
		this->maxEnergyPoints = 0;
}

int ScavTrap::getLevel() const
{
	return level;
}

void ScavTrap::setLevel(int level)
{
	ScavTrap::level = level;
}

int ScavTrap::getMeleeAttackDamage() const
{
	return meleeAttackDamage;
}

void ScavTrap::setMeleeAttackDamage(int meleeAttackDamage)
{
	ScavTrap::meleeAttackDamage = meleeAttackDamage;
}

int ScavTrap::getRangedAttackDamage() const
{
	return rangedAttackDamage;
}

void ScavTrap::setRangedAttackDamage(int rangedAttackDamage)
{
	ScavTrap::rangedAttackDamage = rangedAttackDamage;
}

int ScavTrap::getArmorDamagaeReduction() const
{
	return armorDamagaeReduction;
}

void ScavTrap::setArmorDamagaeReduction(int armorDamagaeReduction)
{
	ScavTrap::armorDamagaeReduction = armorDamagaeReduction;
}

const std::string &ScavTrap::getName() const
{
	return name;
}

void ScavTrap::setName(const std::string &name)
{
	ScavTrap::name = name;
}


void ScavTrap::rangedAttack(const std::string &target)
{
	std::cout << "SC4V-TP " << name << " attacks " << target <<
			  " at range, causing " << rangedAttackDamage << " points of damage!" << std::endl;
}

void ScavTrap::meleeAttack(const std::string &target)
{
	std::cout << "SC4V-TP " << name << " attacks " << target <<
			  " with a melee, causing " << meleeAttackDamage << " points of damage!" << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount)
{
	int __hitPoints;
	if (hitPoints == 0)
		std::cout << "SC4V-TP " << name << " can't take more damage!" << std::endl;
	else if ((armorDamagaeReduction > amount))
		std::cout << "SC4V-TP " << name << " don't feel anything!" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + armorDamagaeReduction - amount) < 0)
			__hitPoints = 0;
		std::cout << "SC4V-TP " << name << " takes " << amount <<" points of damage!" << std::endl;
		setHitPoints(__hitPoints);
	}
}

void ScavTrap::beRepaired(unsigned int amount)
{
	int __hitPoints;

	if (hitPoints == maxHitPoints)
		std::cout << "SC4V-TP " << name << " already full" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + amount) > maxHitPoints)
			__hitPoints = maxHitPoints;
		std::cout << "SC4V-TP " << name << " was repaired by " << amount << " and now has " << __hitPoints << " hit points!" << std::endl;
		setHitPoints(__hitPoints);
	}
}

void ScavTrap::challengeNewcomer()
{
	std::string challenges[] = {
		"find G-spot", "unfusten a bra",
		"satisfy", "kill Hitler" };
	if (energyPoints > 0)
	{
		energyPoints -= 25;
		std::cout << "SC4V-TP " << name << "tries to " << challenges[std::rand() % 4] << " and fails" << std::endl;
	}
	else
		std::cout << "SC4V-TP " << name << " has no energy!" << std::endl;
}

