//
// Created by Dmitry Titenko on 11/3/17.
//

#include <iostream>
#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap()
{
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamagaeReduction = 0;
	type = "NINJ-TP";
	std::cout << "NinjaTrap constructor called" << std::endl;
}

NinjaTrap::NinjaTrap(const std::string &name) : ClapTrap(name)
{
	hitPoints = 60;
	maxHitPoints = 60;
	energyPoints = 120;
	maxEnergyPoints = 120;
	level = 1;
	meleeAttackDamage = 60;
	rangedAttackDamage = 5;
	armorDamagaeReduction = 0;
	type = "NINJ-TP";
	std::cout << "NinjaTrap constructor called" << std::endl;
}

NinjaTrap::NinjaTrap(const NinjaTrap &rhs) : ClapTrap(rhs)
{
	std::cout << "NinjaTrap constructor called" << std::endl;
}

NinjaTrap::~NinjaTrap()
{
	std::cout << "NinjaTrap destructor called" << std::endl;
}


NinjaTrap &NinjaTrap::operator=(const NinjaTrap &rhs)
{
	ClapTrap::operator=(rhs);
	return (*this);
}

void NinjaTrap::ninjaShoebox(const ClapTrap &target)
{
	std::cout << type << " " << name << " makes Air Yautja Strike to " << target.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(const FragTrap &target)
{
	std::cout << type << " " << name << " makes Close Yautja Strike to " << target.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(const ScavTrap &target)
{
	std::cout << type << " " << name << " makes Far Yautja Strike to " << target.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(const NinjaTrap &target)
{
	std::cout << type << " " << name << " makes CONTAAAAACT to " << target.name << std::endl;
}
