//
// Created by Dmitry Titenko on 11/2/17.
//

#ifndef D03_FRAGTRAP_HPP
#define D03_FRAGTRAP_HPP


#include <string>
#include "ClapTrap.hpp"

class FragTrap : public virtual ClapTrap
{
	public:
		FragTrap();
		FragTrap(const FragTrap &);
		FragTrap(const std::string &);
		~FragTrap();
		void vaulthunter_dot_exe(std::string const & target);
		FragTrap &operator=(const FragTrap &rhs);
};


#endif //D03_FRAGTRAP_HPP
