//
// Created by Dmitry Titenko on 11/3/17.
//

#include <iostream>
#include "SuperTrap.hpp"

SuperTrap::SuperTrap() : NinjaTrap(), FragTrap()
{
	hitPoints = (100), maxHitPoints = (100);
	level = (1), type = ("SUPR-TP");
	energyPoints = 120, maxEnergyPoints = 120;
	meleeAttackDamage = 60, rangedAttackDamage = 20,
	armorDamagaeReduction = 5;
	std::cout << "SuperTrap constructor called" << std::endl;
}

SuperTrap::SuperTrap(const SuperTrap &rhs) : NinjaTrap(rhs), FragTrap(rhs)
{
	std::cout << "SuperTrap constructor called" << std::endl;
}

SuperTrap::SuperTrap(const std::string &name) : NinjaTrap(name), FragTrap(name)
{
	hitPoints = (100), maxHitPoints = (100);
	level = (1), type = ("SUPR-TP");
	energyPoints = 120, maxEnergyPoints = 120;
	meleeAttackDamage = 60, rangedAttackDamage = 20,
	armorDamagaeReduction = 5;
	std::cout << "SuperTrap constructor called" << std::endl;
}

void SuperTrap::meleeAttack(const std::string &target)
{
	NinjaTrap::meleeAttack(target);
}

void SuperTrap::rangedAttack(const std::string &target)
{
	FragTrap::rangedAttack(target);
}

SuperTrap::~SuperTrap()
{
	std::cout << "SuperTrap destructor called" << std::endl;
}

SuperTrap &SuperTrap::operator=(const SuperTrap &rhs)
{
	NinjaTrap::operator=(rhs);
	FragTrap::operator=(rhs);
	return *this;
}
