//
// Created by Dmitry Titenko on 11/3/17.
//

#ifndef D03_SUPERTRAP_HPP
#define D03_SUPERTRAP_HPP


#include "NinjaTrap.hpp"

class SuperTrap: public NinjaTrap, public FragTrap
{
	public:
		SuperTrap();
		SuperTrap(const std::string &name);
		SuperTrap(const SuperTrap &);
		~SuperTrap();
		SuperTrap &operator=(const SuperTrap &);
		void rangedAttack(const std::string &target);
		void meleeAttack(const std::string &target);
};


#endif //D03_SUPERTRAP_HPP
