//
// Created by Dmitry Titenko on 11/2/17.
//

#include <iostream>
#include "FragTrap.hpp"

FragTrap::FragTrap() : ClapTrap()
{
	energyPoints = 100;
	energyPoints = (100), maxEnergyPoints = (100);
	meleeAttackDamage = (30), rangedAttackDamage = (20);
	armorDamagaeReduction = (5), type = ("FR4G-TP");
	std::cout << "FragTrap constructor called" << std::endl;
}

FragTrap::FragTrap(const std::string &name) : ClapTrap(name)
{
	energyPoints = 100;
	energyPoints = (100), maxEnergyPoints = (100);
	meleeAttackDamage = (30), rangedAttackDamage = (20);
	armorDamagaeReduction = (5), type = ("FR4G-TP");
	std::cout << "FragTrap constructor called" << std::endl;
}

FragTrap::FragTrap(const FragTrap &rhs) : ClapTrap(rhs)
{
	std::cout << "FragTrap constructor called" << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << "FragTrap destructor called" << std::endl;
}

FragTrap &FragTrap::operator=(const FragTrap &rhs)
{
	ClapTrap::operator=(rhs);
	return (*this);
}

void FragTrap::vaulthunter_dot_exe(std::string const &target)
{
	std::string attacks[] = {"Hat Spin", "Dive Kick", "Hat Trick", "Buzz Saw", "Flip Kick"};
	if (energyPoints > 0)
	{
		std::cout << type << " "<< name << " attacks " << target << " with a "
				  << attacks[std::rand() % 5] << "!!!" << std::endl;
		setEnergyPoints(energyPoints - 25);
	}
	else
		std::cout << type << " " << name << " has no energy!" << std::endl;

}
