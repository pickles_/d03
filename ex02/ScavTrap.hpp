//
// Created by Dmitry Titenko on 11/3/17.
//

#ifndef D03_SCAVTRAP_HPP
#define D03_SCAVTRAP_HPP


#include <string>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
	public:
		ScavTrap();
		ScavTrap(const ScavTrap &);
		ScavTrap(const std::string &);
		virtual  ~ScavTrap();
		ScavTrap &operator=(const ScavTrap &rhs);
		void challengeNewcomer();
};


#endif //D03_SCAVTRAP_HPP
