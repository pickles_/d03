//
// Created by Dmitry Titenko on 11/3/17.
//

#include "ScavTrap.hpp"
#include <iostream>

ScavTrap::ScavTrap():ClapTrap()
{
	energyPoints = (50), maxEnergyPoints = (50);
	meleeAttackDamage = (20), rangedAttackDamage = (15);
	armorDamagaeReduction = (3); type = "SC4V-TP";
	std::cout << "ScavTrap constructor called" << std::endl;
}

ScavTrap::ScavTrap(const std::string &name) : ClapTrap(name)
{
	energyPoints = (50), maxEnergyPoints = (50);
	meleeAttackDamage = (20), rangedAttackDamage = (15);
	armorDamagaeReduction = (3); type = "SC4V-TP";
	std::cout << "ScavTrap constructor called" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap &rhs) : ClapTrap(rhs)
{
	std::cout << "ScavTrap constructor called" << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << "ScavTrap destructor called" << std::endl;
}

ScavTrap &ScavTrap::operator=(const ScavTrap &rhs)
{
	ClapTrap::operator=(rhs);
	return (*this);
}

void ScavTrap::challengeNewcomer()
{
	std::string challenges[] = {
		"find G-spot", "unfusten a bra",
		"satisfy", "kill Hitler" };
	if (energyPoints > 0)
	{
		energyPoints -= 25;
		std::cout << "SC4V-TP " << name << " tries to " << challenges[std::rand() % 4] << " and fails" << std::endl;
	}
	else
		std::cout << "SC4V-TP " << name << " has no energy!" << std::endl;
}

