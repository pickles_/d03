//
// Created by Dmitry Titenko on 11/3/17.
//

#include <iostream>
#include "ClapTrap.hpp"

ClapTrap::ClapTrap() : hitPoints(100), maxHitPoints(100),
					   level(1), type("CL4P-TP")
{
	std::cout << "ClapTrap constructor called" << std::endl;
}

ClapTrap::ClapTrap(const std::string &name) :
	hitPoints(100), maxHitPoints(100),
	name(name), type("CL4P-TP")
{
	std::cout << "ClapTrap constructor called" << std::endl;
}

ClapTrap::ClapTrap(const ClapTrap &rhs)
{
	std::cout << "ClapTrap constructor called" << std::endl;
	*this = rhs;
}

ClapTrap::~ClapTrap()
{
	std::cout << "ClapTrap destructor called" << std::endl;
}

ClapTrap &ClapTrap::operator=(const ClapTrap &rhs)
{
	if (this != &rhs)
	{
		hitPoints = rhs.hitPoints;
		maxHitPoints = rhs.maxHitPoints;
		energyPoints = rhs.energyPoints;
		maxEnergyPoints = rhs.maxEnergyPoints;
		level = rhs.level;
		meleeAttackDamage = rhs.meleeAttackDamage;
		rangedAttackDamage = rhs.rangedAttackDamage;
		armorDamagaeReduction = rhs.armorDamagaeReduction;
		name = rhs.name;
		type = rhs.type;
	}
	return *this;
}

int ClapTrap::getHitPoints() const
{
	return hitPoints;
}

void ClapTrap::setHitPoints(int hitPoints)
{
	ClapTrap::hitPoints = hitPoints;
}

int ClapTrap::getMaxHitPoints() const
{
	return maxHitPoints;
}

void ClapTrap::setMaxHitPoints(int maxHitPoints)
{
	ClapTrap::maxHitPoints = maxHitPoints;
}

int ClapTrap::getEnergyPoints() const
{
	return energyPoints;
}

void ClapTrap::setEnergyPoints(int energyPoints)
{
	ClapTrap::energyPoints = energyPoints;
}

int ClapTrap::getMaxEnergyPoints() const
{
	return maxEnergyPoints;
}

void ClapTrap::setMaxEnergyPoints(int maxEnergyPoints)
{
	ClapTrap::maxEnergyPoints = maxEnergyPoints;
	if (this->maxEnergyPoints < 0)
		this->maxEnergyPoints = 0;
}

int ClapTrap::getLevel() const
{
	return level;
}

void ClapTrap::setLevel(int level)
{
	ClapTrap::level = level;
}

int ClapTrap::getMeleeAttackDamage() const
{
	return meleeAttackDamage;
}

void ClapTrap::setMeleeAttackDamage(int meleeAttackDamage)
{
	ClapTrap::meleeAttackDamage = meleeAttackDamage;
}

int ClapTrap::getRangedAttackDamage() const
{
	return rangedAttackDamage;
}

void ClapTrap::setRangedAttackDamage(int rangedAttackDamage)
{
	ClapTrap::rangedAttackDamage = rangedAttackDamage;
}

int ClapTrap::getArmorDamagaeReduction() const
{
	return armorDamagaeReduction;
}

void ClapTrap::setArmorDamagaeReduction(int armorDamagaeReduction)
{
	ClapTrap::armorDamagaeReduction = armorDamagaeReduction;
}

const std::string &ClapTrap::getName() const
{
	return name;
}

void ClapTrap::setName(const std::string &name)
{
	ClapTrap::name = name;
}


void ClapTrap::rangedAttack(const std::string &target)
{
	std::cout << type << " " << name << " attacks " << target <<
			  " at range, causing " << rangedAttackDamage << " points of damage!" << std::endl;
}

void ClapTrap::meleeAttack(const std::string &target)
{
	std::cout << type << " " << name << " attacks " << target <<
			  " with a melee, causing " << meleeAttackDamage << " points of damage!" << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount)
{
	int __hitPoints;
	if (hitPoints == 0)
		std::cout << type << " " << name << " can't take more damage!" << std::endl;
	else if ((armorDamagaeReduction > amount))
		std::cout << type << " " << name << " don't feel anything!" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + armorDamagaeReduction - amount) < 0)
			__hitPoints = 0;
		std::cout << type << " " << name << " takes " << amount <<" points of damage!" << std::endl;
		setHitPoints(__hitPoints);
	}
}

void ClapTrap::beRepaired(unsigned int amount)
{
	int __hitPoints;

	if (hitPoints == maxHitPoints)
		std::cout << type << " " << name << " already full" << std::endl;
	else
	{
		if ((__hitPoints = hitPoints + amount) > maxHitPoints)
			__hitPoints = maxHitPoints;
		std::cout << type << " " << name << " was repaired by " << amount << " and now has " << __hitPoints << " hit points!" << std::endl;
		setHitPoints(__hitPoints);
	}
}
