//
// Created by Dmitry Titenko on 11/3/17.
//

#ifndef D03_CLAPTRAP_HPP
#define D03_CLAPTRAP_HPP


#include <string>

class ClapTrap
{

	protected:
		int hitPoints, maxHitPoints, energyPoints, maxEnergyPoints, level,
			meleeAttackDamage, rangedAttackDamage, armorDamagaeReduction;
		std::string name, type;

	public:
		ClapTrap();
		ClapTrap(const std::string &name);
		ClapTrap(const ClapTrap &);
		virtual ~ClapTrap();
		ClapTrap &operator=(const ClapTrap &);

		void rangedAttack(const std::string &target);
		void meleeAttack(const std::string &target);
		void takeDamage(unsigned int amount);
		void beRepaired(unsigned int amount);

		//Getters and Setters
		int getHitPoints() const;

		void setHitPoints(int hitPoints);

		int getMaxHitPoints() const;

		void setMaxHitPoints(int maxHitPoints);

		int getEnergyPoints() const;

		void setEnergyPoints(int energyPoints);

		int getMaxEnergyPoints() const;

		void setMaxEnergyPoints(int maxEnergyPoints);

		int getLevel() const;

		void setLevel(int level);

		int getMeleeAttackDamage() const;

		void setMeleeAttackDamage(int meleeAttackDamage);

		int getRangedAttackDamage() const;

		void setRangedAttackDamage(int rangedAttackDamage);

		int getArmorDamagaeReduction() const;

		void setArmorDamagaeReduction(int armorDamagaeReduction);

		const std::string &getName() const;

		void setName(const std::string &name);
};


#endif //D03_CLAPTRAP_HPP
