//
// Created by Dmitry Titenko on 11/2/17.
//

#include "FragTrap.hpp"

int main()
{
	FragTrap f0("Bichara");
	FragTrap f1(f0);
	FragTrap f3("Sup kon'achiy");

	f1.setName("Sin sobaki");

	srand(time(NULL));

	f0.rangedAttack(f3.getName());
	f3.takeDamage(f0.getRangedAttackDamage());
	f3.meleeAttack(f0.getName());
	f0.takeDamage(f3.getMeleeAttackDamage());

	f0.beRepaired(20);
	f3.beRepaired(100);
	f0.vaulthunter_dot_exe(f1.getName());
	f1.takeDamage(105);
	f0.vaulthunter_dot_exe(f3.getName());
	f3.takeDamage(55);
	f0.vaulthunter_dot_exe((f1.getName()));
	f1.takeDamage(55);
	f0.vaulthunter_dot_exe(f3.getName());
	f3.takeDamage(55);
	f0.vaulthunter_dot_exe(f1.getName());
	f3.takeDamage(55);
}
