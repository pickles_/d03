//
// Created by Dmitry Titenko on 11/3/17.
//

#ifndef D03_NINJATRAP_HPP
#define D03_NINJATRAP_HPP


#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap
{
	public:
		NinjaTrap();
		NinjaTrap(const std::string &name);
		NinjaTrap(const NinjaTrap &);
		NinjaTrap &operator=(const NinjaTrap &);

		virtual ~NinjaTrap();


		void ninjaShoebox(const FragTrap &);
		void ninjaShoebox(const ScavTrap &);
		void ninjaShoebox(const ClapTrap &);
		void ninjaShoebox(const NinjaTrap &);
};


#endif //D03_NINJATRAP_HPP
